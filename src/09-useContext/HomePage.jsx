import { useContext } from "react";
import { UserContext } from "./context/UserContext";


export const HomePage = () => {
    //asi se usa el useContext
    const {user} = useContext(UserContext);
  return (
    <>
        <h1>HomePage</h1>
        <hr />
        <pre>
            {JSON.stringify(user, null, 3)}
        </pre>
        
    </>
  )
}
