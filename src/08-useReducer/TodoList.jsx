import { TodoItem } from "./TodoItem"


export const TodoList = ({todos, onDeleteTodo, onToggleTodo}) => {
  
  return (
    <ul className="list-groups">
        {todos.map(todo =>{
            return(
                <TodoItem key={todo.id} todo={todo} onDeleteTodo={onDeleteTodo} onToggleTodo={onToggleTodo}  />
            )
        })}
    </ul>
  )
}
