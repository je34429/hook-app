import { useEffect, useReducer } from "react"
import { useTodos } from "../hooks/useTodos"
import { TodoAdd } from "./TodoAdd"
import { TodoList } from "./TodoList"



export const TodoApp = () =>{
    
    //useTodo
    // todos, handleDeleteTodo, handleNewTodo, handleToggleTodo

    const {todos, todosCount, pendingTodosCount, handleDeleteTodo, handleNewTodo, handleToggleTodo} = useTodos();
    console.log(todos);
    
    //dos formas de usar filter que producen el mismo resultado
    console.log(todos.filter((todo)=>{
        if(!todo.done){
            return todo;
        }
        
    }).length);

    console.log(todos.filter(todo=>!todo.done).length);

    return(
    <>
        <h1>Todos: {todosCount} | <small> pendientes: {pendingTodosCount}</small></h1> 
        <hr/>
        <div className="row">
            <div className="col-7">
                {/* todoList */}
                <TodoList todos={todos} onDeleteTodo={handleDeleteTodo} onToggleTodo={handleToggleTodo}/>
            </div>
            <div className="col-5">
                <h4>Agregar TODO</h4>
                <hr/>
                {/* TodoAdd onNewTodo(todo) */}
                {/* {id: new Date()..., description:lorem ipsum, done:false} */}
                <TodoAdd onNewTodo={handleNewTodo}/>
            </div>
        </div>
    </>
    )
}