
import { useForm } from "../hooks/useForm";

export const TodoAdd = ({onNewTodo}) => {
     const {description, onInputChange, onResetForm,} = useForm({
        description:''
    });
    const onSubmitForm=(e)=>{
        e.preventDefault();
        if(description.length <= 1) return;
        const newTodo = {
            id: new Date().getTime(),
            description: description,
            done:false
        }
        onNewTodo(newTodo);
        onResetForm();
    }

  return (
    <form
    onSubmit={onSubmitForm}>
        <input
            type="text"
            name="description"
            placeholder="¿Qué hay que hacer?"
            className="form-control"
            onChange={onInputChange}
            value={description}
        />
        <button
            type="submit"
            className="btn btn-outline-primary mt-1"
        >Agregar</button>
    </form>
  )
}
