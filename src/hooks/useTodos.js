import { useEffect, useReducer } from 'react'
import { todoReducer } from '../08-useReducer/todoReducer';

export const useTodos = () => {

  const initialTodos =[ ];

  const init = () =>{
      return JSON.parse(localStorage.getItem('todos'))|| [];
  }
  const [todos, dispatch] = useReducer(todoReducer, initialTodos, init)

    useEffect(() => {
        let mounted = true;
        if(mounted){
            localStorage.setItem('todos', JSON.stringify(todos));
        }

        return () => {
            mounted = false;
        }
    }, [todos])
    
    const handleNewTodo = (todo) => {
        const action ={
            type:'[TODO] Add Todo',
            payload:todo
        }
        dispatch(action);

    };

    const handleDeleteTodo = (id) =>{
        const action ={
            type:'[TODO] Delete Todo',
            payload:id
        }
        dispatch(action);
    }

    const handleToggleTodo = (id) =>{
        console.log('toggle');
        const action ={
            type:'[TODO] Toggle Todo',
            payload:id
        }
        dispatch(action);
    }

    const todosCount = todos.length;
    const pendingTodosCount = todos.filter(todo=>!todo.done).length;

    return{
      todos,
      handleNewTodo,
      handleDeleteTodo,
      handleToggleTodo,
      todosCount,
      pendingTodosCount
    }
  
}
